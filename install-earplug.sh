#!/bin/sh

echo '#!/bin/bash
array=($(cat /proc/acpi/wakeup | awk '\''{print $1}'\'' | grep -E "EHC|XHC"))
for i in ${array[@]}
do
	echo $i > /proc/acpi/wakeup
done'  > /usr/local/bin/earplug

chmod +x /usr/local/bin/earplug

echo '[Unit] 
Description=For a better sleep...

[Service] 
Type=oneshot
RemainAfterExit=yes
ExecStart=/usr/local/bin/earplug

[Install] 
WantedBy=multi-user.target' > /etc/systemd/system/earplug.service

systemctl daemon-reload
systemctl enable earplug.service
systemctl start earplug.service

cat /proc/acpi/wakeup
