## How it works
Most GNU/Linux distributions are configured to resume from suspend by USB device input such as mouse move by default. This could be annoying in certain use cases.

Fortunately, there is an easy way to tweak this behavior. You can write device name (e.g. EHC1) to /proc/acpi/wakeup and the feature will toggle on/off. Note that /proc/acpi/wakeup is not a ordinary "file" (but a "device file"). 

This tiny script is to toggle them off on boot.

## How to install

Just execute install-earplug.sh as root.

```
# sh install-earplug.sh
```

## Troubleshooting
At first confirm if the service is working correctly. This script requires systemd, bash and awk to work.

```
# systemctl status earplug.service
```

This script will send all common device names. Although it is suitable for most environments (since the system will simply ignore it if the device is not exist), check if it works as expected in case of trouble.

You can see the current state by showing /proc/acpi/wakeup.

```
# cat /proc/acpi/wakeup
```

If it says "*disabled", system will never resume by input from that bus. If it says "*enabled", something went wrong. Add suspicious device names to /usr/local/bin/earplug if not exist. Try removing device name if it is on /usr/local/bin/earplug and still "*enabled".
